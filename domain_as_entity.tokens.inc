<?php

/**
 * @file
 * All tokens related code should be placed here.
 */

/**
 * Implements hook_token_info().
 */
function domain_as_entity_token_info() {
  $token_types = [
    'domain' => [
      'name' => t('Domain'),
      'description' => t('Tokens related to domain entity.'),
    ],
    'current-domain' => [
      'name' => t('Current domain'),
      'description' => t('Tokens related to current domain entity.'),
    ],
  ];

  $tokens = [
    'domain' => [],
    'current-domain' => [],
  ];

  // Make domains fields available as tokens.
  $fields = field_info_fields();
  if (!empty($fields)) {
    $entity_type = 'domain';
    foreach ($fields as $field) {
      if (in_array($entity_type, array_keys($field['bundles']))) {
        foreach ($field['bundles'][$entity_type] as $bundle) {
          $field_instance = field_info_instance($entity_type, $field['field_name'], $bundle);
          $token_name = $field['field_name'];
          if (!isset($tokens[$entity_type][$token_name])) {
            foreach ($token_types as $token_type_name => $token_type) {
              $tokens[$token_type_name][$token_name] = [
                'name' => $field_instance['label'],
                'description' => $field_instance['description'],
                'module' => 'domain_as_entity',
              ];
            }
          }
        }
      }
    }
  }

  return [
    'types' => $token_types,
    'tokens' => $tokens,
  ];
}

/**
 * Implements hook_tokens().
 */
function domain_as_entity_tokens($type, $tokens, array $data = [], array $options = []) {
  global $language;
  $replacements = [];

  if ($type == 'domain' && !empty($data[$type])) {
    $domain = $data[$type];
    foreach ($tokens as $name => $original) {
      // Fill field tokens from entity.
      if (strpos($name, 'field_') === 0) {
        $field_output = field_view_field($type, $domain, $name, 'token', $language->language);
        $field_output['#token_options'] = $options;
        $field_output['#pre_render'][] = 'token_pre_render_field_token';
        $replacements[$original] = drupal_render($field_output);
      }
    }
  }

  if ($type == 'current-domain') {
    $current_domain = domain_as_entity_get_domain();
    $replacements += token_generate('domain', $tokens, ['domain' => $current_domain], $options);
  }

  return $replacements;
}
