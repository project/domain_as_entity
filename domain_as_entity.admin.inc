<?php

/**
 * @file
 * Administration forms.
 */

/**
 * Generates the domain type editing form.
 *
 * @param array $form
 *   Form.
 * @param array $form_state
 *   Form state.
 * @param \Domain $domain
 *   Domain.
 * @param string $op
 *   Operation.
 *
 * @return array
 *   Built form.
 */
function domain_form(array $form, array &$form_state, Domain $domain, $op = 'edit') {

  if ($op == 'clone') {
    // Only label is provided for cloned entities.
    $domain->label .= ' (cloned)';
  }

  $form['label'] = [
    '#title' => t('Label'),
    '#type' => 'textfield',
    '#required' => TRUE,
    '#default_value' => isset($domain->label) ? $domain->label : '',
    '#weight' => -20,
  ];

  $form['domain'] = [
    '#title' => t('Domain'),
    '#type' => 'textfield',
    '#required' => TRUE,
    '#default_value' => isset($domain->domain) ? $domain->domain : '',
    '#weight' => -19,
  ];

  $form['is_https'] = [
    '#type' => 'checkbox',
    '#title' => t('Is HTTPS'),
    '#description' => t('Set this option if you want switch domain schema to HTTPS.'),
    '#default_value' => !empty($domain->is_https),
  ];

  $form['status'] = [
    '#title' => t('Status'),
    '#description' => t('Set domain to active state when this option checked.'),
    '#type' => 'checkbox',
    '#default_value' => isset($domain->status) ? $domain->status : DOMAIN_AS_ENTITY_DOMAIN_ACTIVE,
  ];

  field_attach_form('domain', $domain, $form, $form_state);

  if (!domain_as_entity_is_default($domain)) {
    $default_domain_did = variable_get('domain_as_entity_default_domain_did', FALSE);
    $form['make_default'] = [
      '#title' => t('Make default'),
      '#type' => 'checkbox',
      '#default_value' => FALSE,
      '#weight' => 35,
    ];

    // If there is no default domain - force current to be default.
    if (!$default_domain_did) {
      $form['make_default']['#default_value'] = TRUE;
      $form['make_default']['#type'] = 'value';
    }
  }

  $form['actions'] = ['#type' => 'actions'];
  $form['actions']['submit'] = [
    '#type' => 'submit',
    '#value' => t('Save domain'),
    '#weight' => 40,
  ];

  return $form;
}

/**
 * Form API submit callback for the type form.
 *
 * @param array $form
 *   Input form.
 * @param array $form_state
 *   Input form state.
 */
function domain_form_submit(array $form, array &$form_state) {
  /** @var Domain $domain */
  $domain = entity_ui_form_submit_build_entity($form, $form_state);
  // Save and go back.
  $domain->save();

  if (!empty($form_state['values']['make_default'])) {
    variable_set('domain_as_entity_default_domain_did', $domain->did);
  }

  $form_state['redirect'] = 'admin/structure/domains';
}

/**
 * Form API validate callback for the type form.
 *
 * @param array $form
 *   Input form.
 * @param array $form_state
 *   Input form state.
 */
function domain_form_validate(array $form, array &$form_state) {
  if (!$form_state['values']['status']) {
    // We can't resave default domain as disabled.
    if (domain_as_entity_is_default($form_state['domain'])) {
      form_error($form['status'], t("You can't make default domain inactive. If you wan't to disable this domain first choose new default domain."));
    }
    // Check if we try to save disabled domain as default.
    if (!empty($form_state['values']['make_default'])) {
      form_error($form['make_default'], t("You can't make domain default if it's inactive. If you want to make this domain default you should make it enabled."));
    }
  }
}
