<?php

/**
 * @file
 * Check if we are on default domain.
 */

/**
 * Plugins are described by creating a $plugin array which will be used
 * by the system that includes this file.
 */
$plugin = [
  'title' => t("Domain: is default"),
  'description' => t('Check if current is the default one'),
  'callback' => 'domain_as_entity_default_domain_access_check',
  'default' => [],
  'summary' => 'domain_as_entity_default_domain_ctools_access_summary',
  'required context' => [],
];

/**
 * Check for access.
 */
function domain_as_entity_default_domain_access_check($conf, $context) {
  return domain_as_entity_is_default();
}

/**
 * Describe an instance of this plugin.
 */
function domain_as_entity_default_domain_ctools_access_summary($conf, $context) {
  return t('Check if current is the default one');
}
