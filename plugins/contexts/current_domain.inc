<?php

/**
 * @file
 *
 * Plugin to provide a current domain context
 */

/**
 * Plugins are described by creating a $plugin array which will be used
 * by the system that includes this file.
 */
$plugin = [
  'title' => t('Current domain'),
  'description' => t('A context automatically detects current domain.'),
  'context' => 'domain_as_entity_context_create_current_domain',
  'edit form' => 'domain_as_entity_context_current_domain_settings_form',
  'defaults' => '',
  'keyword' => 'current_domain',
  'no ui' => FALSE,
  'context name' => 'current_domain',
  'convert list' => 'domain_as_entity_context_current_domain_convert_list',
  'convert' => 'domain_as_entity_context_current_domain_convert',
];

/**
 * It's important to remember that $conf is optional here, because contexts
 * are not always created from the UI.
 *
 * @param $empty
 * @param null $data
 * @param bool|FALSE $conf
 *
 * @return \ctools_context
 */
function domain_as_entity_context_create_current_domain($empty, $data = NULL, $conf = FALSE) {
  $context = new ctools_context('current_domain');
  $context->plugin = 'current_domain';

  if ($empty) {
    return $context;
  }

  $current_domain = domain_as_entity_get_domain();
  if ($current_domain) {
    // Support the array storage from the settings form but also handle direct input from arguments.
    $context->data = $current_domain;
    $context->title = $conf ? check_plain($data['identifier']) : check_plain($data);
    return $context;
  }

  return NULL;
}

/**
 * Provide a list of ways that this context can be converted to a string.
 *
 * @return array
 */
function domain_as_entity_context_current_domain_convert_list() {
  $list = [];
  $tokens = token_info();
  foreach ($tokens['tokens']['domain'] as $id => $info) {
    if (!isset($list[$id])) {
      $list[$id] = $info['name'];
    }
  }

  return $list;
}

/**
 * Convert a context into a string.
 *
 * @param $context
 * @param $type
 * @return mixed
 */
function domain_as_entity_context_current_domain_convert($context, $type) {
  $tokens = token_info();
  if (isset($tokens['tokens']['domain'][$type])) {
    $values = token_generate('domain', [$type => $type], ['domain' => $context->data]);
    if (isset($values[$type])) {
      return $values[$type];
    }
  }

  return NULL;
}

/**
 * Context settings form.
 *
 * @param $form
 * @param $form_state
 * @return mixed
 */
function domain_as_entity_context_current_domain_settings_form($form, &$form_state) {
  return $form;
}

/**
 * Context settings form submit handler.
 *
 * @param $form
 * @param $form_state
 */
function domain_as_entity_context_current_domain_settings_form_submit($form, &$form_state) {

}
