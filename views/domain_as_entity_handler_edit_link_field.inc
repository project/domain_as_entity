<?php

/**
 * @file
 * Handler class definition.
 */

/**
 * Provide field handler for edit link.
 */
class domain_as_entity_handler_edit_link_field extends views_handler_field {
  function construct() {
    parent::construct();
  }

  function query() {
    $this->ensure_my_table();
    $this->add_additional_fields();
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $form['text'] = [
      '#type' => 'textfield',
      '#title' => t('Text to display'),
      '#default_value' => $this->options['text'],
    ];
  }

  function option_definition() {
    $options = parent::option_definition();

    $options['text'] = ['default' => '', 'translatable' => TRUE];

    return $options;
  }

  function render($values) {
    $domain_id = $values->did;

    if (!domain_as_entity_domain_access('update') && !domain_as_entity_domain_access('administer domain entities')) {
      return FALSE;
    }

    $text = !empty($this->options['text']) ? $this->options['text'] : t('edit');

    return l($text, 'admin/structure/domains/manage/' . $domain_id);
  }
}
