<?php

/**
 * @file
 * Views module integration hooks.
 */

/**
 * Implements hook_views_data_alter().
 */
function domain_as_entity_views_data_alter(&$data) {
  $data['domains']['status'] = array(
    'title' => t('Status'),
    'help' => t('Status of the domain (active/inactive)'),
    'field' => array(
      'handler' => 'views_handler_field_boolean',
      'click sortable' => TRUE,
      'output formats' => array(
        'published-notpublished' => array(t('Active'), t('Not active')),
      ),
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_boolean_operator',
      'label' => t('Active'),
      'type' => 'yes-no',
      'use equal' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  $data['domains']['edit'] = array(
    'field' => array(
      'title' => t('Edit'),
      'help' => t('Provide a link to edit domain'),
      'handler' => 'domain_as_entity_handler_edit_link_field',
    ),
  );

  $data['domains']['delete'] = array(
    'field' => array(
      'title' => t('Delete'),
      'help' => t('Provide a link to delete domain'),
      'handler' => 'domain_as_entity_handler_delete_link_field',
    ),
  );

  $data['domains']['operations'] = array(
    'field' => array(
      'title' => t('Operations links'),
      'help' => t('Display all operations available for this domain'),
      'handler' => 'domain_as_entity_handler_operations_field',
    ),
  );

  return $data;
}
