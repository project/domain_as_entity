<?php

/**
 * @file
 * Handler class definition.
 */

/**
 * This field handler aggregates operations that can be done on a model
 * under a single field providing a more flexible way to present them in a view
 */
class domain_as_entity_handler_operations_field extends views_handler_field {
  function construct() {
    parent::construct();

    $this->additional_fields['did'] = 'did';
  }

  function query() {
    $this->ensure_my_table();
    $this->add_additional_fields();
  }

  function render($values) {
    $links = menu_contextual_links('domain_as_entity', 'admin/structure/domains/manage', [$this->get_value($values, 'did')]);
    
    if (!empty($links)) {
      return theme('links', ['links' => $links, 'attributes' => ['class' => ['links', 'inline', 'operations']]]);
    }
  }
}
