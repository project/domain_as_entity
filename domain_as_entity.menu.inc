<?php

/**
 * @file
 * Menu links related logic should be placed here.
 */

/**
 * Implements hook_form_FORM_ID_alter().
 */
function domain_as_entity_form_menu_edit_item_alter(&$form, &$form_state, $form_id) {
  // Add integration with menu links.
  $domain_list = [];
  $menu_link = menu_link_load($form['mlid']['#value']);
  // Load all domains and prepare options array.
  $domains = entity_load_multiple_by_name('domain', FALSE);
  foreach ($domains as $did => $domain) {
    $domain_list[$did] = $domain->label;
  }

  $form['domain'] = [
    '#type' => 'select',
    '#title' => t('Related domain'),
    '#description' => t('Choose domain which you want to be binded to this menu link.'),
    '#options' => [0 => t('- Select a domain -')] + $domain_list,
    '#default_value' => !empty($menu_link['options']['item_attributes']['domain'])
      ? $menu_link['options']['item_attributes']['domain']
      : 0,
    '#states' => [
      'disabled' => [
        "input[name='default_domain']" => array('checked' => TRUE),
      ]
    ]
  ];

  $form['default_domain'] = [
    '#type' => 'checkbox',
    '#title' => t('Default domain'),
    '#description' => t('Check this option if you want bind default domain to this menu link.'),
    '#default_value' => !empty($menu_link['options']['item_attributes']['default_domain'])
      ? $menu_link['options']['item_attributes']['default_domain']
      : 0,
  ];

  array_unshift($form['#submit'], 'domain_as_entity_menu_edit_item_submit');
}

/**
 * Custom form submit to modify menu link before saving.
 *
 * @param array $form
 *   Input form.
 * @param array $form_state
 *   Input form state.
 */
function domain_as_entity_menu_edit_item_submit(array $form, array &$form_state) {
  $form_state['values']['options']['item_attributes']['domain'] = $form_state['values']['domain'];
  $form_state['values']['options']['item_attributes']['default_domain'] = $form_state['values']['default_domain'];
  // Allow customize menu link via hook_translated_menu_link_alter.
  $form_state['values']['options']['alter'] = TRUE;
}

/**
 * Implements hook_translated_menu_link_alter().
 */
function domain_as_entity_translated_menu_link_alter(&$item, $map) {
  $load_domain = FALSE;

  // If we have a domain as setting we should alter result href.
  if (!empty($item['options']['item_attributes']['default_domain'])) {
    $load_domain = TRUE;
    $domain = domain_as_entity_get_default_domain();
  }
  elseif (!empty($item['options']['item_attributes']['domain'])) {
    $load_domain = TRUE;
    $domain = domain_as_entity_load($item['options']['item_attributes']['domain']);
  }

  if (!empty($domain)) {
    $item['href'] = $domain->url($item['href'] == '<front>' ? '' : $item['href']);
    $item['options']['absolute'] = TRUE;
    $item['localized_options']['absolute'] = TRUE;
  }
  // Write log if we tried to load domain but failed.
  elseif ($load_domain) {
    watchdog('domain_as_entity', 'Failed to load domain entity for menu link "%link"', ['%link' => $item['href']], WATCHDOG_ERROR);
    return;
  }
}
