INTRODUCTION
------------
Domain as entity XML sitemap module extends XML Sitemap module for integration
with Domain as entity.

INSTRUCTION
------------
1. Enable module.
2. Create new sitemap and select domain on sitemap creation form.
3. Use hook MODULE_query_xmlsitemap_generate_alter in your custom module for
   adding conditions depending on selected domain.
